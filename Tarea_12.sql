/* Tarea 12 - Ejercicios propuestos */

/* Ejercicio 1 */
SELECT fechaPago
FROM pa.pago
WHERE fechaPago = '2009';

/* Ejercicio 2 */
SELECT local
FROM ma.personaDestino
WHERE local IS NULL;

/* Ejercicio 3 */
SELECT AVG(montoSol) AS Promedio_MontoSol
FROM pa.pago;

/* Ejercicio 4 */
SELECT montoPago
FROM pa.pago
WHERE montoPago > (SELECT AVG(montoPago) FROM pa.pago);

/* Ejercicio 5 */
SELECT ve.documento.tipoMovimiento
FROM ve.documentoPago
JOIN ve.documento ON ve.documentoPago.documento = ve.documento.documento
WHERE ve.documento.tipoMovimiento > 1;

/* Ejercicio 6 */
SELECT ma.saldosIniciales.cantidad, ma.almacen.nombreAlmacen
FROM ma.saldosIniciales
JOIN ma.almacen ON ma.saldosIniciales.almacen = ma.almacen.almacen;

/* Ejercicios 7 */
SELECT cantidad
FROM ma.saldosIniciales
WHERE cantidad > (SELECT AVG(cantidad) FROM ma.saldosIniciales)
ORDER BY cantidad DESC;

/* Ejercicio 8 */
SELECT *
FROM ve.documento
WHERE vendedor = 3;

/* Ejercicio 16 */
SELECT fechaPago
FROM pa.pago
WHERE fechaPago = '2007';

/* Ejercicio 21 */
SELECT anoPeriodo, documentosCanjeados
FROM ve.documentosCanjeados
WHERE anoPeriodo = '2007';

/* Ejercicio 25 */
SELECT vendedor, fechaPago
FROM pa.pago
WHERE fechaPago = '2009';